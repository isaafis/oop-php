<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');
$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "Cold blodded : " . $sheep->cold_blooded . "<br>"; // "no" 

echo "<br>";

$kodok = new frog("budug");
echo "Name : " . $kodok->name . "<br>"; // "shaun"
echo "Legs : " . $kodok->legs . "<br>"; // 4
echo "Cold blodded : " . $kodok->cold_blooded . "<br>"; // "no" 
echo "Jump : ";
$kodok->jump();
echo "<br>";

echo "<br>";

$sungkong = new ape("kerasakti");
echo "Name : " . $sungkong->name . "<br>"; // "shaun"
echo "Legs : " . $sungkong->legs . "<br>"; // 4
echo "Cold blodded : " . $sungkong->cold_blooded . "<br>"; // "no" 
echo "Yell : ";
$sungkong->yell();
